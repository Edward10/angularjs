var app = angular.module('Dating', []);

app.controller('users', ['$scope', function($scope){
    $scope.info = [
        {
            name: 'Petya',
            introduction: 'I am Petya and I am gigolo…',
            isAdule: true,
            isGigolo: true
        },
        {
            name: 'Vanya',
            introduction: 'I am Vanya and I am good boy!',
            isAdule: true,
            isGigolo: false
        },
        {
            name: 'Lena',
            introduction: 'I am Lena and I am good girl!',
            isAdule: true,
            isGigolo: false
        },
        {
            name: 'Sveta',
            introduction: 'I am Sveta and I am 7 year old)))',
            isAdule: false,
            isGigolo: false
        }
    ];
}]);