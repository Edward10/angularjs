(function() {
    'use strict';

    var app = angular.module('task2', []);

    app.controller('heroes', ['$scope', function($scope){
        $scope.heroes = [{
            label: 'Дед Мороз (подвыпивший)',
            info: 'Персона уставшая, выпившая за НГ на 3-4х корпоративах. Основная функция - составить компанию взрослым за праздничным столом.',
            cost: 50.45,
            img: 'http://detkamonline.ru/media/icons/1/santas-snowball-showdown.jpg',
            dateStart: 1447290000000,
            dateEnd: 1421614800000
        },
        {
            label: 'Дед Мороз (трезвый)',
            info: 'Работает утром, сдержанный и добрый, предназначен для детей. Постоянно ходит со своей внучкой Снегуркой.',
            cost: 150.99,
            img: 'http://online-raskraski.ru/media/icons/color/1/Snegurka-i-Ded-Moroz_small.jpg',
            dateStart: 1450764000000,
            dateEnd: 1425794400000
        },
        {
            label: 'Санта Клаус',
            info: 'Предназначен для разбалованных детей. Прилагается опция англоговорения. Требование к заказчику – наличие сарая или гаража для парковки оленей!',
            cost: 200,
            img: 'http://online-raskraski.ru/media/icons/color/1/ded-moroz-i-nord-olen_small.jpg',
            dateStart: 1450789200000,
            dateEnd: 1452862800000
        }];

        $scope.currentTab = 1;

    }]);

    app.controller('tourOrder', ['$scope', function($scope) {

        $scope.types = [{
            id: 1,
            title: 'Еду с женой'
            },
            {
                id: 2,
                title: 'Еду один'
            }];

        $scope.events = [{
            id: 22,
            typeId: 1,
            title: 'поход в театр'
        },
            {
                id: 23,
                typeId: 1,
                title: 'осмотр достопримечательностей'
            },
            {
                id: 24,
                typeId: 1,
                title: 'прогулка на катамаране'
            },
            {
                id: 25,
                typeId: 2,
                title: 'поход в бар на пляже'
            },
            {
                id: 26,
                typeId: 2,
                title: 'дегустация коньяка и виски'
            },
            {
                id: 27,
                typeId: 2,
                title: 'поход в шашлычную с пивом'
            },
            {
                id: 28,
                typeId: 2,
                title: 'рыбалка'
            }];

        $scope.bars = [{
            id: 10,
            title: 'Голубая устрица'
        },
            {
                id: 11,
                title: 'Розовый кальмар'
            },
            {
                id: 12,
                title: 'У Васи'
            }];

        $scope.orderInfo = {
            selectedEvents: {}
        };

        $scope.submitForm = function() {
            console.log('Submitted');
        }

    }]);

})();